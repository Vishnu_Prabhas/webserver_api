// Web Server Using Express

const express = require('express');
const app = express();

// create a middleware and use
app.use(express.json());

const courses = [
    { id: 1, name: 'Probability' },
    { id: 2, name: 'Statistics'},
    { id: 3, name: 'Machine Learning'},
    { id: 4, name: 'Deep Learning'},
    { id: 5, name: 'Artificial Intelligence'},
];

app.get('/', (req, res) => {
    res.send('Hello Folks, this is Express!');
});

app.get('/api/courses', (req, res) => {
    res.send(courses);
});

// /api/courses/1
app.get('/api/courses/:id', (req, res) => {
    let course = courses.find(c => c.id === parseInt(req.params.id));
    if (!course) res.status(404).send('The course with the given ID was not found.');
    res.send(course);
});

// data to be entered 
app.post('/api/courses', (req, res) => {
    // input validation
    // look for npm joi package for complex validation
    if (!req.body.name || req.body.name.length < 3) {
        // 400 bad request
        res.status(400).send('Check Name, min 3 char required');
        return;
    }

    const course = {
        id: courses.length + 1,
        name: req.body.name
    };
    courses.push(course);
    res.send(course);
});

// Listening to port / env
const port = process.env.PORT || 3000;
app.listen(port, 
    () => console.log(`Running on port ${port}...`));








